# Gitlab Proof

[Verifying my OpenPGP key: openpgp4fpr:447F2C325CCED5E40502DB911AAFFA97EBCFBEF6]

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://docs.keyoxide.org/advanced/openpgp-proofs/
